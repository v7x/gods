package heap

import (
	"math/rand"
)

type MeldableNode interface {
	Left() *MeldableNode
	Right() *MeldableNode
	Parent() *MeldableNode
}

type IntMeldableNode struct {
	left, right, parent *IntMeldableNode
	val                 *int
}

func (n *IntMeldableNode) Left() *IntMeldableNode {
	return n.left
}

func (n *IntMeldableNode) Right() *IntMeldableNode {
	return n.right
}

func (n *IntMeldableNode) Parent() *IntMeldableNode {
	return n.parent
}

type MeldableHeap interface {
	Merge(MeldableHeap)
	Add(interface{})
	Remove() interface{}
}

type IntMeldableHeap struct {
	root *IntMeldableNode
	num  int
}

func (h *IntMeldableHeap) merge(n1, n2 *IntMeldableNode) *IntMeldableNode {
	if n1 == nil {
		return n2
	}
	if n2 == nil {
		return n1
	}

	if *n2.val < *n1.val {
		n1, n2 = n2, n1
	}

	if rand.Int()%2 == 1 {
		n1.left = h.merge(n1.left, n2)
		n1.left.parent = n1
	} else {
		n1.right = h.merge(n1.right, n2)
		n1.right.parent = n1
	}

	return n1
}

func (h *IntMeldableHeap) Add(x int) bool {
	nn := new(IntMeldableNode)
	nn.val = &x
	h.root = h.merge(nn, h.root)
	h.root.parent = nil
	h.num++
	return true
}

func (h *IntMeldableHeap) Remove() int {
	x := *h.root.val
	h.root = h.merge(h.root.left, h.root.right)
	if h.root != nil {
		h.root.parent = nil
	}
	h.num--
	return x
}
