package heap

type BinaryHeap interface {
	left(int) interface{}
	right(int) interface{}
	parent(int) interface{}

	Add(interface{}) bool
	BubbleUp(int)

	Remove() interface{}
	TrickleDown(int)
}

type IntBinaryHeap struct {
	Arr []int
	Num int
}

func (h *IntBinaryHeap) New() *IntBinaryHeap {
	bh := IntBinaryHeap{Arr: make([]int, 1), Num: 0}
	return &bh
}

func (h *IntBinaryHeap) left(idx int) int {
	return 2*idx + 1
}

func (h *IntBinaryHeap) right(idx int) int {
	return 2 * (idx + 1)
}

func (h *IntBinaryHeap) parent(idx int) int {
	return int((idx - 1) / 2)
}
func (h *IntBinaryHeap) resize() {
	var addsize int
	if h.Num < 1 {
		addsize = 1
	} else {
		addsize = h.Num * 2
	}
	newArr := make([]int, addsize)
	copy(newArr, h.Arr)
	h.Arr = newArr
}

func (h *IntBinaryHeap) Add(x int) bool {
	if len(h.Arr) < h.Num+1 {
		h.resize()
	}

	h.Arr[h.Num] = x
	h.Num++

	h.BubbleUp(h.Num - 1)
	return true
}

func (h *IntBinaryHeap) BubbleUp(idx int) {
	par := h.parent(idx)

	for idx > 0 && h.Arr[idx] < h.Arr[par] {
		h.Arr[idx], h.Arr[par] = h.Arr[par], h.Arr[idx]
		idx = par
		par = h.parent(idx)
	}
}

func (h *IntBinaryHeap) Remove() int {
	x := h.Arr[0]
	h.Arr = h.Arr[1:]
	h.Num--
	h.TrickleDown(0)

	if 3*h.Num < len(h.Arr) {
		h.resize()
	}
	return x
}

func (h *IntBinaryHeap) TrickleDown(idx int) {
	for idx >= 0 {
		check := -1
		R := h.right(idx)

		if R < h.Num && h.Arr[R] < h.Arr[idx] {
			L := h.left(idx)
			if h.Arr[L] < h.Arr[R] {
				check = L
			} else {
				check = R
			}
		} else {
			L := h.left(idx)
			if L < h.Num && h.Arr[L] < h.Arr[idx] {
				check = L
			}
		}

		if check >= 0 {
			h.Arr[check], h.Arr[idx] = h.Arr[idx], h.Arr[check]
		}

		idx = check
	}
}
