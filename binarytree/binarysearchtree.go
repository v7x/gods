package binarytree

type SearchNode interface {
	Node

	AddChild(*Node) bool
}

type IntSearchNode struct {
	IntNode
	val                 *int
	left, right, parent *IntSearchNode
}

func (n *IntSearchNode) Size() int {
	s := 1
	if n.left != nil {
		s += n.left.Size()
	}
	if n.right != nil {
		s += n.right.Size()
	}

	return s
}

func (n *IntSearchNode) AddChild(u *IntSearchNode) bool {
	if *u.val < *n.val {
		n.left = u
	} else if *u.val > *n.val {
		n.right = u
	} else {
		return false
	}
	u.parent = n
	return true
}

func (n *IntSearchNode) New(x int) *IntSearchNode {
	nn := new(IntSearchNode)
	nn.val = &x
	return nn
}

type BinarySearchTree interface {
	BinaryTree

	FindEQ(interface{}) *int
	Find(interface{})
	Add(interface{}) bool
	FindLast(interface{})
	Splice(*SearchNode)
	RemoveNode(*SearchNode) bool
	Remove(interface{}) bool
}

type IntBinarySearchTree struct {
	root *IntSearchNode
	num  int
}

func (t *IntBinarySearchTree) Size() int {
	return t.num
}

func (t *IntBinarySearchTree) FindEQ(x int) *int {
	check := t.root

	for check != nil {
		if x < *check.val {
			check = check.left
		} else if x > *check.val {
			check = check.right
		} else {
			return check.val
		}
	}
	return nil
}

func (t *IntBinarySearchTree) Find(x int) *int {
	check := t.root
	var last *IntSearchNode

	for check != nil {
		if x < *check.val {
			last = check
			check = check.left
		} else if x > *check.val {
			check = check.right
		} else {
			return check.val
		}
	}

	if last == nil {
		return nil
	}
	return last.val
}

func (t *IntBinarySearchTree) Add(x int) bool {
	p := t.FindLast(x)

	nn := new(IntSearchNode)
	nn.val = &x

	if t.root == nil {
		t.root = nn
		t.num++
		return true
	}

	success := p.AddChild(nn)

	if success {
		t.num++
	}

	return success

}

func (t *IntBinarySearchTree) FindLast(x int) *IntSearchNode {
	check := t.root
	var prev *IntSearchNode

	for check != nil {
		prev = check

		if x < *check.val {
			check = check.left
		} else if x > *check.val {
			check = check.right
		} else {
			return check
		}
	}
	return prev
}

func (t *IntBinarySearchTree) Splice(n *IntSearchNode) {
	var sNode, par *IntSearchNode

	if n.left != nil {
		sNode = n.left
	} else {
		sNode = n.right
	}

	if n == t.root {
		t.root = sNode
	} else {
		par = n.parent
		if par.left == n {
			par.left = sNode
		} else {
			par.right = sNode
		}
	}

	if sNode != nil {
		sNode.parent = par
	}
	t.num--
}

func (t *IntBinarySearchTree) RemoveNode(n *IntSearchNode) {
	if n.left == nil || n.right == nil {
		t.Splice(n)
	} else {
		check := n.right
		for check.left != nil {
			check = check.left
		}

		n.val = check.val
		t.Splice(check)
	}
}

func (t *IntBinarySearchTree) Remove(x int) bool {
	if t.num == 0 {
		return false
	}
	rn := t.FindLast(x)
	t.RemoveNode(rn)
	return true
}

func (t *IntBinarySearchTree) New() *IntBinarySearchTree {
	tree := new(IntBinarySearchTree)

	return tree
}
