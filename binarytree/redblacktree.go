package binarytree

/*
   Red-Black trees are binary search trees where each node has a color value of
   red or black, hence the name. The color red is represented by a 0, while
   black is represented by a 1. Before and after every operation on a red-black
   tree, the fololowing conditions must be satisfied:

   1. For any node path starting at the root node and ending on a leaf node (a
	node with no children), there are the same number of black nodes as
	there would be on any other path. (black-height property)

   2. An arbitrary red node will only have black nodes as children.
	(no-red-edge property)

   There are multiple ways to implement a red-black tree. Our implementation
   here follows a third rule:

   3. If a node's children are different colors, the red child is on the left
	side. (left-leaning property)

   This property gives the tree its full name, a left-leaning red-black tree,
   or llrb for short.

   The purpose of the first two properties of a red-black tree is to garuntee
   that searches for an arbitrary value in a tree will perform in O(logn) time
   in the worst-case scenario. This is because even in the worst case, a path
   to any leaf node will always have the same number of black nodes and a very
   limited number of red nodes which will always be less than the number of
   black nodes. Even in the absolute worst case, the total length of a path
   will never be greater than twice the length of any other path. The third
   property is introduced to simplify some of the code needed to perform some
   of the operations necessary to maintain the first two rules.
*/

const black = 1
const red = 0

type RedBlackNode interface {
	SearchNode
}

type IntRedBlackNode struct {
	val                 *int
	left, right, parent *IntRedBlackNode
	color               int
}

func (n *IntRedBlackNode) AddChild(u *IntRedBlackNode) bool {
	if *u.val < *n.val {
		n.left = u
	} else if *u.val > *n.val {
		n.right = u
	} else {
		return false
	}
	u.parent = n
	return true
}

type RedBlackTree interface {
	BinarySearchTree

	FlipLeft(*RedBlackNode)
	FlipRight(*RedBlackNode)

	AddNode(*RedBlackNode) bool
	AddFixup(*RedBlackNode)
}

/*
   IntRedBlackTree structs contain a pointer to the root node, an int counting
   the number of nodes in the tree, and a pointer to a "null" node. The null
   IntRedBlackNode acts as a stand-in for nil pointers to children (and in the
   root node's case, parents). The null node is also always colored black. This
   simplifies some of the code by removing the need for any checks for nil and
   only requiring checks for node color.
*/
type IntRedBlackTree struct {
	root, null *IntRedBlackNode
	num        int
}

func (t *IntRedBlackTree) NewNode(x int) *IntRedBlackNode {
	nn := new(IntRedBlackNode)
	nn.val = &x
	nn.color = black
	nn.left, nn.right, nn.parent = t.null, t.null, t.null
	return nn
}

func (t *IntRedBlackTree) New() *IntRedBlackTree {
	rb := new(IntRedBlackTree)
	rb.null = new(IntRedBlackNode)
	rb.null.color = black
	rb.null.left, rb.null.right, rb.null.parent = rb.null, rb.null, rb.null
	rb.root = rb.null
	return rb
}

func (t *IntRedBlackTree) Size() int {
	return t.num
}

/*
   pushBlack pushes a node's color towards red and its children's colors
   towards black.
*/
func (t *IntRedBlackTree) pushBlack(n *IntRedBlackNode) {
	n.color--
	n.left.color++
	n.right.color++
}

/*
   pullBlack is the reverse of pushBlack.
*/
func (t *IntRedBlackTree) pullBlack(n *IntRedBlackNode) {
	n.color++
	n.left.color--
	n.right.color--
}

/*
   flipLeft swaps the colors of a node with its right child and reverses their parent-
   child relationship. Useful in restoring the left-leaning property.
*/
func (t *IntRedBlackTree) flipLeft(n *IntRedBlackNode) {
	n.color, n.right.color = n.right.color, n.color
	t.rotateLeft(n)
}

/*
   rotateLeft is the same as any other binarytree's operation of the same
   name, except any reference to nil is replaced with the sentinel t.null.
*/
func (t *IntRedBlackTree) rotateLeft(n *IntRedBlackNode) {
	rotor := n.right
	rotor.parent = n.parent
	if rotor.parent != t.null {
		if rotor.parent.left == n {
			rotor.parent.left = rotor
		} else {
			rotor.parent.right = rotor
		}
	}

	n.right = rotor.left

	if n.right != t.null {
		n.right.parent = n
	}

	n.parent = rotor
	rotor.left = n

	if n == t.root {
		t.root = rotor
		t.root.parent = t.null
	}
}

/*
   flipRight is symmetric to flipLeft.
*/
func (t *IntRedBlackTree) flipRight(n *IntRedBlackNode) {
	n.color, n.left.color = n.left.color, n.color
	t.rotateRight(n)
}

/*
   rotateRight is symmetric to rotateLeft.
*/
func (t *IntRedBlackTree) rotateRight(n *IntRedBlackNode) {
	rotor := n.left
	rotor.parent = n.parent
	if n.parent != t.null {
		if rotor.parent.left == n {
			rotor.parent.left = rotor
		} else {
			rotor.parent.right = rotor
		}
	}

	n.left = rotor.right

	if n.left != t.null {
		n.left.parent = n
	}

	n.parent = rotor
	rotor.right = n

	if n == t.root {
		t.root = rotor
		t.root.parent = t.null
	}
}

/*
   Add creates a new node with value x performs the standard insert for any
   other binary search tree. It sets the color of the node to red before
   insertion so that it won't ever violate the black-height property. It
   might still violate the no-red-edge and left-leaning property, however,
   so the function addFixup corrects these violations if necessary.
*/
func (t *IntRedBlackTree) Add(x int) bool {
	nn := t.NewNode(x)
	nn.color = red

	if t.AddNode(nn) {
		t.addFixup(nn)
		return true
	}
	return false
}

/*
   AddNode adds a node to the tree. It performs the same operations as for any
   other binary search tree, except that it it references t.null instead of
   nil.
*/
func (t *IntRedBlackTree) AddNode(n *IntRedBlackNode) bool {
	par := t.FindLast(*n.val)
	var res bool
	if t.root == t.null {
		t.root = n
		res = true
	} else {
		res = par.AddChild(n)
	}
	if res {
		t.num++
	}
	return res
}

/*
   FindLast finds the last node that is equal to x or that would be on the
   same path as x were it in the tree.
*/
func (t *IntRedBlackTree) FindLast(x int) *IntRedBlackNode {
	check := t.root
	prev := t.null

	for check != t.null {
		prev = check

		if x < *check.val {
			check = check.left
		} else if x > *check.val {
			check = check.right
		} else {
			return check
		}
	}

	return prev
}

/*
   addFixup runs a loop iterating over nodes towards the root of the tree on
   the condition that the current node's color is red. For each node in its
   path, it first sets the node's color to black if it is the root. Having a
   black node as root ensures the root and its children will never violate the
   no-red-edge constraint. The change doesn't violate the black-height
   constraint either, since the root itself is by nature in every path.

   The loop then find's the current node's parent. If the left child of the
   parent is black, it performs a flipLeft(). This garuntees the left-leaning
   rule is followed. The current node is set to its parent, and the parent
   updated in kind.

   If the (possibly new) parent's color is also black, we return. Because it
   is not possible to have a red-red edge on the next iteration, our work
   is done.

   Finally, we check the parent's parent (the current node's grandparent). If
   the grandparent's right child is black, we perform a flipRight() on the
   grandparent. This swaps the parental relation between the parent and
   grandparent. It also makes sure the (new) grandparent has two red children
   while it itself is black, so we may return.

   If the grandparent's right child was not black, then the original node and
   its parent are both red as we would have returned at this point otherwise.
   To satisfy the no-red-edge property, we can swap the grandparent's and its
   children's colors, set the current node to the grandparent, and continue
   looping.

   There's a reason it took me a month and a half to get this algorithm
   implemented correctly.
*/
func (t *IntRedBlackTree) addFixup(n *IntRedBlackNode) {
	for n.color == red {
		if n == t.root {
			n.color = black
		}

		par := n.parent

		if par.left.color == black {
			t.flipLeft(par)
			n = par
			par = n.parent
		}

		if par.color == black {
			return // red-red edge is eliminated
		}

		grand := par.parent

		if grand.right.color == black {
			t.flipRight(grand)
			return
		}

		t.pushBlack(grand)

		n = grand
	}
}

/*
   Contains simply looks for a node containing x using the FindLast function.
*/
func (t *IntRedBlackTree) Contains(x int) bool {
	find := t.FindLast(x)

	if x == *find.val {
		return true
	} else {
		return false
	}
}

/*
   Remove uses FindLast to follow the path of x were it in the tree. If the
   node found has a value that is not x, we return false as x is not in the
   tree. Otherwise we continue to RemoveNode().
*/
func (t *IntRedBlackTree) Remove(x int) bool {
	rn := Findlast(x)
	if rn.val != x || rn == t.null {
		return false
	}

	return t.RemoveNode(rn)
}

/*
   RemoveNode is the most complicated part of a red-black tree to implement. It
   first finds a node 'rnode' with only one child. It sets n's value to rnode's
   value and then splices rnode. This removes n's original value completely from
   the tree. n's parent is set to rnode's and rnode's color is added to n's.
   This may cause n's color to become higher than black, but that along with
   any other property violations a fixed in removeFixup().
*/
func (t *IntRedBlackTree) RemoveNode(n *IntRedBlackNode) bool {
	rnode := n.right
	if rnode == t.null {
		rnode = n
		n = rnode.left
	} else {
		for rnode.left != t.null {
			rnode = rnode.left
		}
		n.val = rnode.val
		n = rnode.right
	}

	t.splice(rnode)
	n.color += rnode.color
	n.parent = rnode.parent
	t.removeFixup(n)
	return true
}

/*
   removeFixup's complexity will break you. In fact it's so complex it broke
   itself, and consists of four seperate functions. While the color of the
   node n is greater than black (1), we check for four different undesirable
   states the tree might be in. The first, case 0, is to check if the current
   node n is the root node. If so, we set its color to black. Setting the root
   node to black will never violate any of the three properties of a left-
   leaning red-black tree, so this is the easiest way to solve any issues. This
   also immediately terminates the loop.

   The next three cases occur when n's parent's left child is colored red,
   when n is the left child of its parent, and then when n's sibling is black
   and n is the right child (the only other possible undesirable case). The fix
   for each case is complex enough they warrent their own function, and their
   explanation can be found in those function's documentation.

   During the loop, n changes nodes until n refers to the "root" of the subtree
   which was changed. The root of this subtree may have changed color, in
   particular from red to black, so the last portion of the function checks if
   n's parent violates the left-leaning property and fixes it if it does.
*/
func (t *IntRedBlackTree) removeFixup(n *IntRedBlackNode) {
	for n.color > black {
		if n == t.root { // case 0
			n.color = black
		} else if n.parent.left.color == red { // case 1
			n = t.removeFixup1(n)
		} else if n == n.parent.left { // case 2
			n = t.removeFixup2(n)
		} else { // case 3
			n = t.removeFixup3(n)
		}
	}

	if n != t.root {
		check := n.parent
		if check.right.color == red && check.left.color == black {
			t.flipLeft(check)
		}
	}
}

/*
   removeFixup1 resolves case 1: node n's sibling, the left child of n's
   parent, is red. To fix this we perform flipRight() on n's parent and
   proceed to the next iteration of the for loop in removeFixup(). Doing
   a flipRight() causes n's parent to violate the left-leaning property,
   however it also garuntees the next iteration will be case 3 which will
   resolve each of these issues.
*/
func (t *IntRedBlackTree) removeFixup1(n *IntRedBlackNode) *IntRedBlackNode {
	t.flipRight(n.parent)
	return n
}

/*
   removeFixup2 resolves case 2: node n's sibling is black, and n is the left
   child of its parent. We call pullBlack() on n's parent, making n black, n's
   sibling red, and their parent black or double-black. N's parent no longer
   satisfies the left-leaning property, so we do a flipLeft() to fix that.

   N's parent, p, is red and n's sibling (s) is now the root of the subtree.
   We inspect p's right child, q, to ensure the no-red-edge property is not
   violated. If q is black, p satisfies the no-red-edge property and we can
   continue to the next iteration of the for loop in removeFixup() with n = s.

   If q is red, both the no-red-edge and left-leaning properties are violated
   at q and w, respectively. We use rotateLeft() on p to restore left-leaning.
   To recap, q is now the left child of s, p is the left child of q, q and p
   are both red, and s is black or double-black. flipRight() makes q the
   parent of s and p. A call to pushBlack() on q sets s and p to black and
   sets q to the original color of p.

   At this point, the double-black node is eliminated and no-red-edge and
   black-height are both restored. However the right child of s may be red,
   so we check and call flipLeft to correct it if necessary.
*/
func (t *IntRedBlackTree) removeFixup2(n *IntRedBlackNode) *IntRedBlackNode {
	par := n.parent
	sib := par.right

	t.pullBlack(par)
	t.flipLeft(par)

	q := par.right

	if q.color == red {
		t.rotateLeft(par)
		t.flipRight(sib)
		t.pushBlack(q)
		if sib.right.color == red {
			t.flipLeft(sib)
		}
		return q
	} else {
		return sib
	}
}

/*
   removeFixup3 resolves case 3: node n's sibling s is black, and n is the
   right child of its parent p. This case is symmetric to case 2 except for
   the fact that the left-leaning property is asymmetric so restoring it
   requires a different method to fix.

   We start with a pullBlack() on p which makes s red and n black. flipRight()
   puts s at the root of the subtree. p is now red, and the code finishes in
   one of two ways depending on on the color of p's left child q.

   If q is red, the code finishes the same way as case 2, although there is no
   danger of s violating the left-leaning property (we wouldn't be in case 3
   if there was) so we don't need to check.

   If q is black, we examine the color of s's left child. If it is red, s has
   two red children and the extra black can be pushed down with a pushBlack()
   on s. s is now p's original color and we are finished.

   If s's left child is black, s violates the left-leaning property. We
   flipLeft() to restore this, the return s so that the next iteration of the
   for loop in removeFixup continues with n = s.
*/
func (t *IntRedBlackTree) removeFixup3(n *IntRedBlackNode) *IntRedBlackNode {
	par := n.parent
	sib := par.left

	t.pullBlack(par)
	t.flipRight(par) // par is now red

	q := par.left

	if q.color == red { //q-par is red-red
		t.rotateRight(par)
		t.flipLeft(sib)
		t.pushBlack(q)
		return q
	} else {
		if sib.left.color == red {
			t.pushBlack(sib)
			return sib
		} else { // ensure left leaning
			t.flipLeft(sib)
			return par
		}
	}
}

/*
   splice performs the same actions as splice() in any other binary search
   tree, except the nil is repleced with t.null instead.
*/
func (t *IntRedBlackTree) splice(n *IntRedBlackNode) {
	var snode, par *IntRedBlackNode

	if n.left != t.null {
		snode = n.left
	} else {
		snode = n.right
	}

	if n == t.root {
		t.root = snode
		par = t.null
	} else {
		par = n.parent
		if par.left == n {
			par.left = snode
		} else {
			par.right = snode
		}
	}

	if snode != t.null {
		snode.parent = par
	}
	t.num--
}
