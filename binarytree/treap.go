package binarytree

import (
	"math/rand"
)

type TreapNode interface {
	SearchNode
	Priority()
}

type IntTreapNode struct {
	val                 *int
	left, right, parent *IntTreapNode
	priority            int
}

func (n *IntTreapNode) Priority() int {
	return n.priority
}

func (n *IntTreapNode) AddChild(u *IntTreapNode) bool {
	if *u.val < *n.val {
		n.left = u
	} else if *u.val > *n.val {
		n.right = u
	} else {
		return false
	}

	u.parent = n
	return true
}

type Treap interface {
	BinarySearchTree

	RotateLeft(*IntTreapNode)
	RotateRight(*IntTreapNode)
	BubbleUp(*IntTreapNode)
	AddNode(*IntTreapNode)
	TrickleDown(*IntTreapNode)
}

type IntTreap struct {
	root *IntTreapNode
	num  int
}

func (t *IntTreap) Size() int {
	return t.num
}

func (t *IntTreap) RotateLeft(n *IntTreapNode) {
	rotor := n.right
	rotor.parent = n.parent

	if rotor.parent != nil {
		if rotor.parent.left == n {
			rotor.parent.left = rotor
		} else {
			rotor.parent.right = rotor
		}
	}

	n.right = rotor.left

	if n.right != nil {
		n.right.parent = n
	}

	n.parent = rotor
	rotor.left = n

	if n == t.root {
		t.root = rotor
		t.root.parent = nil
	}
}

func (t *IntTreap) RotateRight(n *IntTreapNode) {
	rotor := n.left
	rotor.parent = n.parent

	if rotor.parent != nil {
		if rotor.parent.left == n {
			rotor.parent.left = rotor
		} else {
			rotor.parent.right = rotor
		}
	}

	n.left = rotor.right

	if n.left != nil {
		n.left.parent = n
	}

	n.parent = rotor
	rotor.right = n

	if n == t.root {
		t.root = rotor
		t.root.parent = nil
	}
}

func (t *IntTreap) Add(x int) bool {
	nn := new(IntTreapNode)
	nn.priority = rand.Int()
	nn.val = &x

	if t.AddNode(nn) {
		t.BubbleUp(nn)
		t.num++
		return true
	}
	return false
}

func (t *IntTreap) AddNode(nn *IntTreapNode) bool {
	last := t.FindLast(*nn.val)
	var added bool

	if last == nil {
		t.root = nn
		added = true
	} else {
		added = last.AddChild(nn)
	}

	return added
}

func (t *IntTreap) FindLast(x int) *IntTreapNode {
	check := t.root
	var prev *IntTreapNode

	for check != nil {
		prev = check

		if x < *check.val {
			check = check.left
		} else if x > *check.val {
			check = check.right
		} else {
			return check
		}
	}

	return prev
}

func (t *IntTreap) BubbleUp(n *IntTreapNode) {
	for n != t.root && n.parent.priority > n.priority {
		if n.parent.right == n {
			t.RotateLeft(n.parent)
		} else {
			t.RotateRight(n.parent)
		}
	}

	if n.parent == nil {
		t.root = n
	}
}

func (t *IntTreap) Remove(x int) bool {

	if t.Size() <= 0 {
		return false
	}

	last := t.FindLast(x)

	if last != nil && *last.val == x {
		t.TrickleDown(last)
		t.Splice(last)
		return true
	}

	return false
}

func (t *IntTreap) Splice(n *IntTreapNode) {
	var snode, par *IntTreapNode

	if n.left != nil {
		snode = n.left
	} else {
		snode = n.right
	}

	if n == t.root {
		t.root = snode
	} else {
		par = n.parent
		if par.left == n {
			par.left = snode
		} else {
			par.right = snode
		}
	}

	if snode != nil {
		snode.parent = par
	}

	t.num--
}

func (t *IntTreap) TrickleDown(n *IntTreapNode) {
	for n.left != nil || n.right != nil {
		if n.left == nil {
			t.RotateLeft(n)
		} else if n.right == nil {
			t.RotateRight(n)
		} else if n.left.Priority() < n.right.Priority() {
			t.RotateRight(n)
		} else {
			t.RotateLeft(n)
		}

		if t.root == n {
			t.root = n.parent
		}
	}
}

func (t *IntTreap) New() *IntTreap {
	return new(IntTreap)
}
