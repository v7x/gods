package binarytree

func IntMax(x, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}

/*
Node Interface
*/
type Node interface {
	Left() *Node
	Right() *Node
	Parent() *Node

	Depth() int
	Height() int
	Size() int

	Traverse()
}

/*
Int Node
*/
type IntNode struct {
	val                 *int
	left, right, parent *IntNode
}

func (n *IntNode) Left() *IntNode {
	return n.left
}

func (n *IntNode) Rigth() *IntNode {
	return n.right
}

func (n *IntNode) Parent() *IntNode {
	return n.parent
}

//Depth counts the number of Nodes between n and a Node with nil as its parent.
func (n *IntNode) Depth() int {
	retNode := n
	d := 0
	for retNode.parent != nil {
		retNode = retNode.parent
		d++
	}

	return d
}

//Height recursively finds the highest number of Nodes between n and the
//children of a Node whose children are both nil.
func (n *IntNode) Height() int {
	if n.left == nil && n.right == nil {
		return 1
	} else if n.left == nil && n.right != nil {
		return 1 + n.right.Height()
	} else if n.left != nil && n.right == nil {
		return 1 + n.left.Height()
	} else {
		return 1 + IntMax(n.left.Height(), n.right.Height())
	}
}

//Size recursively finds the number of Nodes of which n is an ancestor.
func (n *IntNode) Size() int {
	s := 1
	if n.left != nil {
		s += n.left.Size()
	}
	if n.right != nil {
		s += n.left.Size()
	}
	return s
}

//Traverese recursively traverses though each Node of which n is an ancestor.
func (n *IntNode) Traverse() {
	if n.left != nil {
		n.left.Traverse()
	}

	if n.right != nil {
		n.right.Traverse()
	}
	return
}

/*
IntNode constructor
*/
func (n *IntNode) New() *IntNode {
	return new(IntNode)
}

/*
Binary Tree interface
*/
type BinaryTree interface {
	Root() *Node
	SetRoot(*Node)

	Size() int
	Traverse()
	BFTraverse()
	New()
}

/*
Int Binary Tree
*/
type IntBinaryTree struct {
	root *IntNode
}

func (b *IntBinaryTree) Root() *IntNode {
	return b.root
}

func (b *IntBinaryTree) SetRoot(n *IntNode) {
	b.root = n
}

func (b *IntBinaryTree) Size() int {
	u := b.root
	var prev, next *IntNode
	n := 0

	for u != nil {
		if prev == u.parent {
			n++
			if u.left != nil {
				next = u.left
			} else if u.right != nil {
				next = u.right
			} else {
				next = u.parent
			}
		} else if prev == u.left {
			if u.right != nil {
				next = u.right
			} else {
				next = u.parent
			}
		} else {
			next = u.parent
		}

		prev = u
		u = next
	}

	return n
}

func (b *IntBinaryTree) Traverse() {
	u := b.root
	var next, prev *IntNode

	for u != nil {
		if prev == u.parent {
			if u.left != nil {
				next = u.left
			} else if u.right != nil {
				next = u.right
			} else {
				next = u.parent
			}
		} else if prev == u.left {
			if u.right != nil {
				next = u.right
			} else {
				next = u.parent
			}
		} else {
			next = u.parent
		}
		prev = u
		u = next
	}
}

func (b *IntBinaryTree) BFTraverse() {
	q := []IntNode{}

	if b.root == nil {
		q = append(q, *b.root)
	}

	// thanks to https://github.com/golang/go/wiki/SliceTricks for
	// push and pop
	for len(q) > 0 {
		var u IntNode
		u, q = q[len(q)-1], q[:len(q)-1] // pop
		if u.left != nil {
			q = append([]IntNode{*u.left}, q...) // push front
		}
		if u.left != nil {
			q = append([]IntNode{*u.right}, q...) // push front
		}
	}
}

/*
IntBinaryTree constructor
*/
func (t *IntBinaryTree) New() *IntBinaryTree {
	bt := new(IntBinaryTree)
	return bt
}
