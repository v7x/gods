package binarytree

import (
	"math"
)

const logof32 = 0.4054651081081644 //math.Log(3.0/2)

func log32(n int) int {
	return int(math.Log(float64(n)) / logof32)
}

type ScapegoatTree interface {
	BinarySearchTree

	Rebuild(*IntSearchNode)
	PackIntoSlice(*IntSearchNode, []*IntSearchNode, int)
	BuildBalance([]*IntSearchNode, int, int)
}

type IntScapegoatTree struct {
	IntBinarySearchTree
	count int
}

func (t *IntScapegoatTree) New() *IntScapegoatTree {
	sg := new(IntScapegoatTree)
	return sg
}

func (t *IntScapegoatTree) Rebuild(n *IntSearchNode) {
	if n == nil {
		return
	}

	ns := n.Size()
	par := n.parent

	arr := make([]*IntSearchNode, ns)
	t.PackIntoSlice(n, arr, 0)

	if par == nil {
		t.root = t.BuildBalanced(arr, 0, ns)
		t.root.parent = nil
	} else if par.right == n {
		par.right = t.BuildBalanced(arr, 0, ns)
		par.right.parent = par
	} else {
		par.left = t.BuildBalanced(arr, 0, ns)
		par.left.parent = par
	}
}

func (t *IntScapegoatTree) PackIntoSlice(n *IntSearchNode, arr []*IntSearchNode, i int) int {
	if n == nil {
		return i
	}

	i = t.PackIntoSlice(n.left, arr, i)
	arr[i] = n
	i++

	return t.PackIntoSlice(n.right, arr, i)
}

func (t *IntScapegoatTree) BuildBalanced(arr []*IntSearchNode, i int, ns int) *IntSearchNode {
	if ns == 0 {
		return nil
	}

	half := ns / 2

	arr[i+half].left = t.BuildBalanced(arr, i, half)
	if arr[i+half].left != nil {
		arr[i+half].left.parent = arr[i+half]
	}

	arr[i+half].right = t.BuildBalanced(arr, i+half+1, ns-half-1)
	if arr[i+half].right != nil {
		arr[i+half].right.parent = arr[i+half]
	}

	return arr[i+half]
}

func (t *IntScapegoatTree) Add(x int) bool {
	nn, deep := t.addWithDepth(x)

	if nn != nil {
		if deep {
			par := nn.parent
			for 3*par.Size() <= 2*par.parent.Size() {
				par = par.parent
			}
			t.Rebuild(par.parent)
		}
		return true

	} else {
		return false
	}
}

func (t *IntScapegoatTree) addWithDepth(x int) (*IntSearchNode, bool) {
	p := t.FindLast(x)

	nn := new(IntSearchNode)
	nn.val = &x

	added := false

	if t.root == nil {
		t.root = nn
		added = true
	} else {
		added = p.AddChild(nn)
	}

	if added {
		t.num++
		t.count++
		if nn.Depth() > log32(t.count) {
			return nn, true
		} else {
			return nn, false
		}
	} else {
		return nil, false
	}
}

func (t *IntScapegoatTree) Remove(x int) bool {
	if t.num == 0 {
		return false
	}

	t.superRemove(x)
	if 2*t.num < t.count {
		t.Rebuild(t.root)
		t.count = t.num
	}
	return true
}

func (t *IntScapegoatTree) superRemove(x int) {
	rn := t.FindLast(x)
	t.RemoveNode(rn)
}
