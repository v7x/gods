package demo

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/v7x/gods/binarytree"
)

func BinarySearchTreeDemo(data []int) int {
	datasize := len(data)

	bt := new(binarytree.IntBinarySearchTree)
	bt = bt.New()

	var time1, time2, total int64
	var added, removed bool

	add, remove := 0, 0

	for i := 0; i < datasize; i++ {
		time1 = time.Now().UnixNano()
		added = bt.Add(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if added {
			add++
		}
	}
	avg := float64(total) / float64(datasize)
	size := bt.Size()

	fmt.Println("Values added:", add)
	fmt.Println("Values not added:", datasize-add)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of Binary Search Tree:", size)
	rand.Shuffle(len(data), func(i, j int) {
		data[i], data[j] = data[j], data[i]
	})

	//c := 0
	total = 0

	for i := datasize - 1; i >= 0; i-- {
		//fmt.Println(c)
		time1 = time.Now().UnixNano()
		removed = bt.Remove(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if removed {
			remove++
		}
		//c++
	}

	avg = float64(total) / float64(datasize)

	fmt.Println("Values removed:", remove)
	fmt.Println("Values not removed:", datasize-remove)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of emptied Binary Search Tree:", bt.Size())

	return 0
}

func TreapDemo(data []int) int {
	datasize := len(data)

	tr := new(binarytree.IntTreap)
	tr = tr.New()

	var time1, time2, total int64
	var added, removed bool

	add, remove := 0, 0

	//d := 0
	for i := 0; i < datasize; i++ {
		//fmt.Println(d)
		time1 = time.Now().UnixNano()
		added = tr.Add(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if added {
			add++
		}
		//d++
	}
	avg := float64(total) / float64(datasize)
	size := tr.Size()

	fmt.Println("Values added:", add)
	fmt.Println("Values not added:", datasize-add)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of Treap:", size)

	rand.Shuffle(len(data), func(i, j int) {
		data[i], data[j] = data[j], data[i]
	})

	//c := 0
	total = 0

	for i := datasize - 1; i >= 0; i-- {
		//fmt.Println(c)
		time1 = time.Now().UnixNano()
		removed = tr.Remove(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if removed {
			remove++
		}
		//c++
	}
	avg = float64(total) / float64(datasize)

	fmt.Println("Values removed:", remove)
	fmt.Println("Values not removed:", datasize-remove)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of emptied Treap:", tr.Size())

	return 0
}

func ScapegoatTreeDemo(data []int) int {
	datasize := len(data)

	sg := new(binarytree.IntScapegoatTree)
	sg = sg.New()

	var time1, time2, total int64
	var added, removed bool

	add, remove := 0, 0

	for i := 0; i < datasize; i++ {
		time1 = time.Now().UnixNano()
		added = sg.Add(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if added {
			add++
		}
	}
	avg := float64(total) / float64(datasize)
	size := sg.Size()

	fmt.Println("Values added:", add)
	fmt.Println("Values not added:", datasize-add)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of Scapegoat Tree:", size)

	rand.Shuffle(len(data), func(i, j int) {
		data[i], data[j] = data[j], data[i]
	})

	total = 0
	//c := 0

	for i := datasize - 1; i >= 0; i-- {
		//fmt.Println(c)
		time1 = time.Now().UnixNano()
		removed = sg.Remove(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if removed {
			remove++
		}
		//c++
	}
	avg = float64(total) / float64(datasize)

	fmt.Println("Values removed:", remove)
	fmt.Println("Values not removed:", datasize-remove)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of emptied Scapegoat Tree:", sg.Size())

	return 0
}

func RedBlackTreeDemo(data []int) int {
	datasize := len(data)

	rb := new(binarytree.IntRedBlackTree)
	rb = rb.New()

	var time1, time2, total int64
	var added, removed bool

	add, remove := 0, 0

	//c := 0
	for i := 0; i < datasize; i++ {
		//fmt.Println(c)
		time1 = time.Now().UnixNano()
		added = rb.Add(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if added {
			add++
		}
		//c++
	}
	avg := float64(total) / float64(datasize)
	size := rb.Size()

	fmt.Println("Values added:", add)
	fmt.Println("Values not added:", datasize-add)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of Red Black Tree:", size)

	rand.Shuffle(len(data), func(i, j int) {
		data[i], data[j] = data[j], data[i]
	})

	total = 0
	//d := 0
	for i := datasize - 1; i >= 0; i-- {
		//fmt.Println(d)
		time1 = time.Now().UnixNano()
		removed = rb.Remove(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if removed {
			remove++
		}
		//d++

	}
	avg = float64(total) / float64(datasize)

	fmt.Println("Values removed:", remove)
	fmt.Println("Values not removed:", datasize-remove)
	fmt.Println("Total time:", total, "ns")
	fmt.Println("Average time:", avg, "ns")

	fmt.Println("Size of emptied Red Black Tree:", rb.Size())

	return 0
}

func BinaryTreeDemo() {
	fmt.Println("Start of Binary Tree demo.")
	sizes := [5]int{100, 1000, 10000, 100000, 1000000} //, 10000000}
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < len(sizes); i++ {

		data := make([]int, sizes[i])
		datarange := sizes[i] * 10

		for i := 0; i < len(data); i++ {
			data[i] = rand.Intn(datarange)
		}

		fmt.Println("Binary Search Tree:", sizes[i], "nodes")
		BinarySearchTreeDemo(data)

		fmt.Println("\n")

		fmt.Println("Treap:", sizes[i], "nodes")
		TreapDemo(data)

		fmt.Println("\n")

		fmt.Println("Scapegoat Tree:", sizes[i], "nodes")
		ScapegoatTreeDemo(data)

		fmt.Println("\n")

		fmt.Println("Red Black Tree:", sizes[i], "nodes")
		RedBlackTreeDemo(data)

		fmt.Println("\n")
	}

	fmt.Println("End of Binary Tree Demo")
}
