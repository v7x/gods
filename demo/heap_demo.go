package demo

import (
	"fmt"
	"gitlab.com/v7x/gods/heap"
	"math/rand"
	"time"
)

func BinaryHeapDemo(data []int) {

	bh := new(heap.IntBinaryHeap)
	bh = bh.New()

	var time1, time2, total int64

	var added bool
	add := 0

	for i := 0; i < len(data); i++ {
		time1 = time.Now().UnixNano()
		added = bh.Add(data[i])
		time2 = time.Now().UnixNano()
		total += time2 - time1
		if added {
			add++
		}
	}
	avg := float64(total) / float64(len(data))

	fmt.Println("Values added:", add)
	fmt.Println("Values not added:", len(data)-add)
	fmt.Println("Total time to add data:", total, "ns")
	fmt.Println("Average time to add data:", avg, "ns")

	rand.Shuffle(len(data), func(i, j int) {
		data[i], data[j] = data[j], data[i]
	})
	var removed int
	remove := 0

	for i := len(data) - 1; i >= 0; i-- {
		time1 = time.Now().UnixNano()
		removed = bh.Remove()
		time2 = time.Now().UnixNano()
		total += time2 - time1
		data[i] = removed
		remove++
	}
	avg = float64(total) / float64(len(data))

	fmt.Println("Values removed:", remove)
	fmt.Println("Values not removed:", len(data)-remove)
	fmt.Println("Total time to remove data:", total, "ns")
	fmt.Println("Average time to remove data:", avg, "ns")
}

func HeapDemo(sizes []int) {

	for i := 0; i < len(sizes); i++ {
		datasize := sizes[i]
		data := datagen(datasize)

		binheap := make([]int, len(data))
		copy(binheap, data)

		BinaryHeapDemo(binheap)
	}

}
