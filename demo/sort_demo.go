package demo

import (
	"fmt"
	"gitlab.com/v7x/gods/sort"
	"math/rand"
	"time"
)

func SortDemo() {

	rand.Seed(time.Now().UnixNano())

	sizes := [5]int{100, 1000, 100000, 1000000, 10000000}

	for s := 0; s < len(sizes); s++ {

		datasize := sizes[s]
		fmt.Println("Sort demo for", datasize, "int arrays.")

		data := make([]int, datasize)
		datarange := datasize * 10

		for i := 0; i < datasize; i++ {
			data[i] = rand.Intn(datarange)
		}

		merge := make([]int, len(data))
		copy(merge, data)

		var time1, time2, total int64

		//fmt.Println("Unsorted data:", merge)

		fmt.Println("Merge Sorting data.")
		time1 = time.Now().UnixNano()
		merge = sort.MergeSort(merge)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Merge sorted data:", merge)
		fmt.Println("Time to merge sort:", total, "ns")

		quick := make([]int, len(data))
		copy(quick, data)

		fmt.Println("Quick sorting data.")
		time1 = time.Now().UnixNano()
		quick = sort.QuickSort(quick)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Quick sorted data:", quick)
		fmt.Println("Time to quick sort:", total, "ns")

		hep := make([]int, len(data))
		copy(hep, data)

		fmt.Println("Heap sorting data.")
		time1 = time.Now().UnixNano()
		hep = sort.HeapSort(hep)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Heap sorted data:", hep)
		fmt.Println("Time to heap sort:", total, "ns")

		count := make([]int, len(data))
		autocount := make([]int, len(data))
		copy(count, data)
		copy(autocount, data)

		max := sort.MaxVal(count)

		fmt.Println("Count sorting data without knowing max value.")
		time1 = time.Now().UnixNano()
		autocount = sort.AutoCountSort(autocount)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Auto-count sorted data:", autocount)
		fmt.Println("Time to auto-count sort data:", total, "ns")

		fmt.Println("Count sorting data when max value is known.")
		time1 = time.Now().UnixNano()
		count = sort.CountSort(count, max)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Count sorted data:", count)
		fmt.Println("Time to count sort data:", total, "ns")

		radix := make([]uint, len(data))

		for i := 0; i < len(data); i++ {
			radix[i] = uint(data[i])
		}

		fmt.Println("Radix sorting data with default int size")
		time1 = time.Now().UnixNano()
		radix = sort.RadixSort(radix)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Radix sorted data:", radix)
		fmt.Println("Time to radix sort data:", total, "ns")

		radix32 := make([]uint32, len(data))

		for i := 0; i < len(data); i++ {
			radix32[i] = uint32(data[i])
		}

		fmt.Println("Radix sorting data with 32 bit integers.")
		time1 = time.Now().UnixNano()
		radix32 = sort.RadixSort32(radix32)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Radix sorted data:", radix32)
		fmt.Println("Time to radix sort data:", total, "ns")

		radix64 := make([]uint64, len(data))

		for i := 0; i < len(data); i++ {
			radix64[i] = uint64(data[i])
		}

		fmt.Println("Radix sorting data with 64 bit integers.")
		time1 = time.Now().UnixNano()
		radix64 = sort.RadixSort64(radix64)
		time2 = time.Now().UnixNano()
		total = time2 - time1
		//fmt.Println("Radix sorted data:", radix64)
		fmt.Println("Time to radix sort data:", total, "ns")
	}
}
