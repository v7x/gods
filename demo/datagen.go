package demo

import (
	"math/rand"
	"time"
)

func datagen(datasize int) []int {
	rand.Seed(time.Now().UnixNano())

	data := make([]int, datasize)

	for i := 0; i < len(data); i++ {
		data[i] = rand.Intn(datasize * 10)
	}

	return data
}
