package main

import (
	"gitlab.com/v7x/gods/demo"
)

func main() {
	sizes := make([]int, 5)
	s := 100
	for i := 0; i < len(sizes); i++ {
		sizes[i] = s
		s *= 10
	}

	//demo.BinaryTreeDemo()
	demo.SortDemo()
	//demo.HeapDemo(sizes)
}
