package main

import (
	"/gods/linkedlist"
	"fmt"
	"math/rand"
	"time"
)

func demo(datasize int) {
	return
}

func main() {

	sizes := []int{100, 1000, 10000, 100000, 1000000}
	for i := 0; i < len(sizes); i++ {
		demo(sizes[i])
	}

	return
}
