package linkedlist

/*
Package linkedlist provides three implementations of the 'linked list' data
structure. The implementations are based on the lessons found at
http://opendatastructures.org/ods-python/ods-python-html.html
*/

import (
	"fmt"
)

//node is the base structure used to build linked lists.
type node struct {
	null       bool
	val        *int
	prev, next *node
}

//Node returns a pointer to a new node.
func Node(i int) *node {
	return &node{
		val: &i,
	}
}

type indexError struct {
	idx int
}

func (e *indexError) Error() string {
	return fmt.Sprintf("Index %d not in list.", e.idx)
}

func IndexError(idx int) error {
	return &indexError{
		idx: idx,
	}
}

//sllist is a singly-linked list. The base struct contains an int 'size'
//to track the number of nodes in the list, and two pointers to the first
//and last nodes, respectively. For each node in a singly-linked list,
//node.prev == nil. For the last node sequentially (pointed to by sllist.tail),
//node.next == nil.
type sllist struct {
	size       int
	head, tail *node
}

//SLList returns a pointer to a zeroed sllist.
func SLList() *sllist {
	return new(sllist)
}

//Size returns the number of nodes in sllist l..
func (l *sllist) Size() int {
	return l.size
}

//Push creates a new node u with a value of int i, sets u.next to the old
//l.head, and sets l.head to u. It then increments l.size by 1.
func (l *sllist) Push(i int) {
	u := Node(i)
	u.next = l.head
	l.head = u

	if l.size == 0 {
		l.tail = u
	}

	l.size++
}

//Pop, after checking that l is not empty, removes l.head by setting l.head to
//l.head.next and decreasing l.size by 1. If the node being removed is also
//l.tail, l.tail is set to nil. We don't need to set l.head to nil, because the
//l.head we just removed had a .next value of nil.
func (l *sllist) Pop() (int, error) {
	if l.size == 0 {
		return 0, IndexError(0)
	}

	val := l.head.val
	l.head = l.head.next
	l.size--

	if l.size == 0 {
		l.tail = nil
	}

	return *val, nil
}

//An sllist can implement FIFO queue operations with remove() and add()
//methods.

func (l *sllist) Remove() (int, error) {
	return l.Pop()
}

func (l *sllist) Add(i int) bool {
	u := Node(i)

	if l.size == 0 {
		l.head = u
	} else {
		l.tail.next = u
	}

	l.tail = u

	l.size++

	return true
}

//dllist is a doubly-linked list. The base structure contains an int 'size'
//to track the number of nodes in the list, and a single pointer to a dummy
//node. Each node in a doubly-linked list contains a pointer to the next and
//previous nodes.
type dllist struct {
	size  int
	dummy *node
}

//DLList returns a pointer to a zeroed dllist. It also prepares the dllist's
//dummy node by setting the .prev and .next pointers to the dummy itself and
//setting the null flag to true. The null flag is used to check whether or not
//a node is a dummy, since dummy nodes cannot hold nil as their value.
func DLList() *dllist {
	dummy := Node(0)
	dummy.next = dummy
	dummy.prev = dummy
	dummy.null = true
	return &dllist{
		dummy: dummy,
	}
}

func (l *dllist) Size() int {
	return l.size
}

//GetNode finds the node at index idx and returns it.
func (l *dllist) GetNode(idx int) *node {
	retnode := l.dummy
	if idx < l.size {
		retnode = l.dummy.next
		for i := 0; i < idx; i++ {
			retnode = retnode.next
		}
	} else {
		retnode = l.dummy
		for i := l.size; i > idx; i-- {
			retnode = retnode.prev
		}
	}
	return retnode
}

//Get finds the node at index idx and returns its value.
func (l *dllist) Get(idx int) int {
	return *l.GetNode(idx).val
}

//Set finds the node at index idx, replaces its value with x, and returns
//the old value of the node at idx.
func (l *dllist) Set(idx, x int) int {
	node := l.GetNode(idx)
	old := *node.val
	node.val = &x
	return old
}

//AddBefore creates a new node with value x and adds it to the list before
//node bef.
func (l *dllist) AddBefore(bef *node, x int) *node {
	node := Node(x)

	node.prev = bef.prev
	node.next = bef

	node.next.prev = node
	node.prev.next = node

	l.size++
	return node
}

//Add uses AddBefore() and Getnode() to add a new node with value x at
//index idx.
func (l *dllist) Add(idx, x int) {
	l.AddBefore(l.GetNode(idx), x)
}

//RemoveNode removes the specified node from the list.
func (l *dllist) RemoveNode(old *node) {
	old.prev.next = old.next
	old.next.prev = old.prev
	l.size--
}

//Remove uses RemoveNode() and GetNode() to remove the node at index idx.
func (l *dllist) Remove(idx int) {
	l.RemoveNode(l.GetNode(idx))
}
