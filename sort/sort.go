package sort

import (
	"gitlab.com/v7x/gods/heap"
	"math/rand"
)

/*
   MergeSort() splits an array 'arr' in half and then recursively performs
   MergeSort() on each half. After each recursive iteration, the two halves
   are then merged together with the merge() function explained below.
*/
func MergeSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}

	half := len(arr) / 2

	arr0 := make([]int, half)
	copy(arr0[0:half], arr[0:half])
	MergeSort(arr0)

	arr1 := make([]int, len(arr)-half)
	copy(arr1[0:len(arr)-half], arr[half:len(arr)])
	MergeSort(arr1)

	merge(arr0, arr1, arr)

	return arr
}

/*
   To merge the arrays 'arr0' and 'arr1', each array has its elements added to
   the array at pointer 'arr' one at a time. If either arr0 or arr1 is empty,
   we add the next elements from the other non-empty array. Otherwise we take
   the smallest element in arr0 and the next element in arr1 and add it to the
   array at arr.
*/
func merge(arr0, arr1, arr []int) {
	i0, i1 := 0, 0

	for i := 0; i < len(arr); i++ {
		if i0 == len(arr0) {
			arr[i] = arr1[i1]
			i1++
		} else if i1 == len(arr1) { // always occurs if arr1 is empty
			arr[i] = arr0[i0]
			i0++
		} else if arr0[i0] <= arr1[i1] {
			arr[i] = arr0[i0]
			i0++
		} else {
			arr[i] = arr1[i1]
			i1++
		}
	}
}

/*
   Quicksort picks a random pivot element 'x' in array 'arr'. It partitions
   arr into the set of elements less than x, equal to x, and greater than x.
   It then recursively sorts the first and third partitions.
*/
func QuickSort(arr []int) []int {
	quicksort(arr, 0, len(arr))
	return arr
}

func quicksort(arr []int, i, num int) []int {
	if num <= 1 {
		return arr
	}

	x := arr[i+rand.Intn(num)]

	ls, eq, gr := i-1, i, i+num

	for eq < gr {
		if arr[eq] < x {
			ls++
			arr[eq], arr[ls] = arr[ls], arr[eq]
			eq++
		} else if arr[eq] > x {
			gr--
			arr[eq], arr[gr] = arr[gr], arr[eq]
		} else {
			eq++
		}
	}

	quicksort(arr, i, ls-i+1)
	quicksort(arr, gr, num-(gr-i))
	return arr
}

/*
   HeapSort takes an array 'arr' and sets it as the array of the BinaryHeap
   'h'. The binary heap then "trickles down" for each index in h.Arr starting
   at h.Arr's halfway point. It swaps each element with the element at index
   0 of h.Arr and "trickles down" again at index 0. h.Arr is then reversed and
   returned as arr. Documentation on the "TrickleDown" method can be found in
   the documentation for the binary heap.
*/
func HeapSort(arr []int) []int {
	h := heap.IntBinaryHeap{Arr: arr, Num: len(arr)}

	half := h.Num / 2

	for i := half - 1; i >= 0; i-- {
		h.TrickleDown(i)
	}

	for h.Num > 1 {
		h.Num--
		h.Arr[h.Num], h.Arr[0] = h.Arr[0], h.Arr[h.Num]
		h.TrickleDown(0)
	}

	arr = h.Arr

	// reverses arr
	for i := (len(arr) / 2) - 1; i >= 0; i-- {
		opp := len(arr) - 1 - i
		arr[i], arr[opp] = arr[opp], arr[i]
	}

	return arr
}

/*
   CountSort (and the following RadixSort) is based on the observation that
   the statement "c[a[i]] = 1" executes in constant time but has len(c)
   possible outcomes depending on the value of a[i]. Because of this, the
   algorithm making such a statement can't be represented with a binary tree
   but as (greater-than-bi)nary trees whose 'nary' prefix is limited only by
   the machine it is run on's memory. As such Count- and RadixSort both run
   much faster than comparison-based sorting algorithms.

   CountSort takes an array 'arr' of integers with values in the range
   0,...,'top'-1. It takes another array 'count' of length 'top' and, for each
   element 'x' in arr, increments count[x] where arr[i] == x. For each value in
   count, arr[n:n+count[i]-1] is then set to i where n is the current number of
   values that have already been set.

   Note that AutoCountSort will take slightly longer since it must first
   determine the largest number in the array before sorting. The algorithm
   to find the maximum value is done in O(n) time, but each operation is short
   so it does not add too much overhead if you don't know the maximum value
   ahead of time.
*/

func AutoCountSort(arr []int) []int {
	return CountSort(arr, MaxVal(arr))
}

func CountSort(arr []int, top int) []int {
	count := make([]int, top+1)

	for i := 0; i < len(arr)-1; i++ {
		count[arr[i]]++
	}

	for i := 1; i <= top; i++ {
		count[i] += count[i-1]
	}

	retarr := make([]int, len(arr))

	for i := len(arr) - 1; i >= 0; i-- {
		count[arr[i]] = count[arr[i]] - 1
		retarr[count[arr[i]]] = arr[i]
	}

	return retarr
}

/*
   RadixSort uses several passes of CountSort to compensate for CountSort's
   one major weakness. While it is efficient for arrays whose maximum values
   are slightly less than the length of the array, CountSort is less efficient
   with wider ranges of values. RadixSort sorts 'width'-bit integers by using
   width/d to sort integers 'd' bits at a time. More precisely, the
   algorithm first sorts the numbers by their least significant d  bits, then
   by the next significant d bits, and so on. 'd' can be any number of bits,
   though it should be a value that balances bits sorted and runs of CountSort.

   An implementation for the default int, int32, and int64 are each supplied.
   The int and int32 implementations both use 8 for the value of d, while the
   int64 one uses 16.
*/

const (
	w   = 32
	w32 = 32
	w64 = 64
	d   = 8
	d32 = 8
	d64 = 16
)

func RadixSort(arr []uint) []uint {
	var retarr []uint

	var p uint
	for p = 0; p < (w / d); p++ {
		count := make([]uint, 1<<d)
		retarr = make([]uint, len(arr))

		for i := 0; i < len(arr); i++ {
			bits := (arr[i] >> d * p) & ((1 << d) - 1)
			count[bits]++
		}

		for i := 1; i < (1 << d); i++ {
			count[i] += count[i-1]
		}

		for i := len(arr) - 1; i >= 0; i-- {
			bits := (arr[i] >> d * p) & ((1 << d) - 1)
			count[bits]--
			retarr[count[bits]] = arr[i]
		}
		arr = retarr
	}
	return retarr

}

func RadixSort32(arr []uint32) []uint32 {
	var retarr []uint32

	var p uint32
	for p = 0; p < (w32 / d32); p++ {
		count := make([]uint32, 1<<d32)
		retarr = make([]uint32, len(arr))

		for i := 0; i < len(arr); i++ {
			bits := (arr[i] >> d32 * p) & ((1 << d32) - 1)
			count[bits]++
		}

		for i := 1; i < (1 << d32); i++ {
			count[i] += count[i-1]
		}

		for i := len(arr) - 1; i >= 0; i-- {
			bits := (arr[i] >> d32 * p) & ((1 << d32) - 1)
			count[bits]--
			retarr[count[bits]] = arr[i]
		}
		arr = retarr
	}
	return retarr

}

func RadixSort64(arr []uint64) []uint64 {
	var retarr []uint64

	var p uint64
	for p = 0; p < (w64 / d64); p++ {
		count := make([]uint64, 1<<d64)
		retarr = make([]uint64, len(arr))

		for i := 0; i < len(arr); i++ {
			bits := (arr[i] >> d64 * p) & ((1 << d64) - 1)
			count[bits]++
		}

		for i := 1; i < (1 << d64); i++ {
			count[i] += count[i-1]
		}

		for i := len(arr) - 1; i >= 0; i-- {
			bits := (arr[i] >> d64 * p) & ((1 << d64) - 1)
			count[bits]--
			retarr[count[bits]] = arr[i]
		}
		arr = retarr
	}
	return retarr

}

// difference-or-zero
func doz(x, y int) int {
	if x >= y {
		return x - y
	} else {
		return 0
	}
}

func Max(x, y int) int {
	return y + doz(x, y)
}

func Min(x, y int) int {
	return x - doz(x, y)
}

func MaxVal(arr []int) int {
	if len(arr) == 1 {
		return arr[0]
	}

	max := arr[0]

	//start i at 1 because we having nothing to check arr[0] against
	for i := 1; i < len(arr); i++ {
		max = Max(max, arr[i])
	}

	return max
}

func MinVal(arr []int) int {
	if len(arr) == 1 {
		return arr[0]
	}

	min := arr[0]

	//start i at 1 because we have nothing to check arr[0] against
	for i := 1; i < len(arr); i++ {
		min = Min(min, arr[i])
	}

	return min
}
